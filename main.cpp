#include "Block.hpp"
#include "Interface.hpp"

int main (int argc, char *argv[]) {
	struct arguments args;
	argp_parse(&argp, argc, argv, 0, 0, &args);

	fstream bpfile(args.bpfilename, fstream::in | fstream::out);
	BPTree<uint64_t, int64_t> ziptree(bpfile, args.fieldlabels, args.fieldsizes, 0);

	//Main UI menu used to make changes to the B+ tree list.
	char choice = 'n';
	string record, pkey;
	while (choice != 'e'){
		cout << "Please enter the option corresponding to your choice." << endl;
		cout << "I - Insert new record to the tree." << endl;
		cout << "R - Remove an existing record from the tree." << endl;
		cout << "D - Display all contents of the tree." << endl;
		cout << "F - Go to the flag edit menu." << endl;
		cout << "g - toggle debug flag. Currently set to: " << args.debug << endl;
		cout << "v - toggle verbose flag. Currently set to: " << args.verbose << endl;
		cout << "E - Exit the program. All unsaved changes will be saved automatically." << endl;

		cin >> choice;
		switch (tolower(choice)){
			case 'i' :
				cin >> record;
				ziptree.insert(record);
				break;
			case 'r' :
				cin >> pkey;
				ziptree.remove(pkey);
				break;
			case 'd' :
				ziptree.dump();
				break;
			case 'g' :
				args.debug = (args.debug) ? false : true;
			case 'v' :
				args.verbose = (args.verbose) ? false : true;
				break;
		}
	}
	return 0;
}

