
all: testgen
	g++ -g main.cpp -o BPTree

testgen:
	g++ makeTestFile.cpp -o testgen

clean:
	rm -f testgen BPTree
