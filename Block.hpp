#include <iostream>
#include <vector>
#include "BPTree.hpp"
using namespace std;

template <typename T, typename RBN> class BPTree;

template<typename T, typename RBN>	//T is the type of records stored in the blocks
class Block{
	public:
		friend BPTree<T,RBN>;
		//Inherited classes need to overload output and input themselves

		BPTree<T,RBN> *bptree;	//Need to access the header
		RBN parent;		//Pointer to parent block
		RBN rbn;		//This block's rbn
}; //end Block class declaration

//Class inherits from Block
template<typename T, typename RBN>
class SEQ_BLK : public Block<T,RBN> {
	public:
		SEQ_BLK();
		SEQ_BLK(fstream &, streampos);
		void add(string);	//Needs to handle splitting and redistributing itself
		void remove(string);
		string pkey(string);
		bool isFull();
		bool isUnderFull();

		template <T,RBN>
		friend ostream& operator<<(ostream &, const SEQ_BLK<T,RBN>&);

		RBN prev;		//Pointer to the previous block
		RBN next;		//Pointer to the next block
		vector<string> records;
		SEQ_BLK<T,RBN> *redist();
		SEQ_BLK<T,RBN> *split();
		void merge();
};

//Class inherits from Block
template<typename T, typename RBN>
class IDX_BLK : public Block<T,RBN> {
	public:
		IDX_BLK();
		IDX_BLK(fstream &, streampos);
		void add_key(T key, RBN rbn);
		
		/*
			* Overloaded Insertion Operator
		 */
		template <T,RBN>
		friend ostream& operator<<(ostream &, const IDX_BLK<T,RBN>&);
		
		/*
			* Sets the parent's RBN
			* @param rbn The new RBN of the parent
		 */
		inline void setParent(const RBN rbn){
			this->parent = rbn;
		}//end setParent
		
		/*
			* Gets the RBN of the parent
			* @return The RBN of the parent
		 */
		inline RBN getParent() const{
			return this->parent;
		}//end getParent
		vector<T> keys;		//Vector of keys
		vector<RBN> rbn;	//Pointers to children
};

//-----------------------Sequence set member functions------------------------

/**
 * Constructs an empty sequence set block.
 */
template<typename T, typename RBN>
SEQ_BLK<T,RBN>::SEQ_BLK(){
	this->parent = prev = next = -1;  //We'll care about the parent when we get to the index set
}

template <typename T, typename RBN>
void SEQ_BLK<T,RBN>::add(string record) {
	if (!isFull()) {
		for (int i = 0; i < records.size(); i++) {
			if (pkey(record) < pkey(records[i])) {
				records.insert(record, i);
				return;
			}
		}
		records.push_back(record);
	} else {
		SEQ_BLK<T,RBN> *alt = NULL;
		if ((alt = redist()) != NULL) {
		} else {
			alt = split();
		}
		//TODO: need to find block (this or alt) to insert record into
	}
}

template <typename T, typename RBN>
void SEQ_BLK<T,RBN>::remove(string key) {
	for (int i = 0; i < records.size(); i++) {
		if (pkey(records[i]) == key) {
			records.erase(i);
		}
	}

	//TODO: Check for underflow to see if we need to redist or merge
}

template <typename T, typename RBN>
SEQ_BLK<T,RBN> *SEQ_BLK<T,RBN>::split() {
	SEQ_BLK<T,RBN> *alt = new SEQ_BLK<T,RBN>();
	alt->next = next;
	alt->prev = this->rbn;
	alt->rbn = this->bptree->newBlock();
	next = alt->rbn;
	for (int i = records.size()/2; i < records.size();) {
		alt->records.push_back(records[i]);
	}

	return alt;
}

template <typename T, typename RBN>
inline bool SEQ_BLK<T,RBN>::isFull() {
	return records.size() == this->bptree->hdr.count.records;
}

/**
 * Returns the primary key of a record.
 */
template <typename T, typename RBN>
string SEQ_BLK<T,RBN>::pkey(string record) {
	return record.substr(this->bptree->hdr.field.pkey_start, this->bptree->hdr.field.pkey_end);
}

/**
BPTree.hpp:194:24: error: conversion from ‘long int’ to non-scalar type ‘SEQ_BLK<long unsigned int, long int>’ requested
	* Overloaded output operator for Sequence Set Blocks
 */	
template <typename T, typename RBN>
ostream& operator<<(ostream &out_s, const SEQ_BLK<T,RBN>& block) {
	out_s << block.records.size() << block.parent << block.prev << block.next;
	for (int i = 0; i < block.records.size(); i++) {
		out_s.write(block.records[i], block.records[i].length);
	}
	return out_s;
}

//----------------------Index set member functions-------------------------

/*
	* Constructs an empty index set block
 */
template<typename T, typename RBN>
IDX_BLK<T,RBN>::IDX_BLK(){
	setParent(-1);
}//end default constructor

/*
	* Constructs an index set block from the specified file stream
	* @param file The fstream object in which the index block is being read from
	* @param pos The position in the file that the index block is located at
 */
template<typename T, typename RBN>
IDX_BLK<T,RBN>::IDX_BLK(fstream & file, streampos pos){
		
	
} //end index set explicit-val constructor

