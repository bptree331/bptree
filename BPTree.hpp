#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <cinttypes>
#include <cstdlib>
#include <cmath>
#include <algorithm>

#ifndef BPTREE
#define BPTREE

using namespace std;

typedef enum ORDER {
	BLK_SIZE, SEQ_SET, IDX_SET
} ORDER;

template<typename T, typename RBN> class Block;
template<typename T, typename RBN> class SEQ_BLK;
template<typename T, typename RBN> class IDX_BLK;

template <typename T, typename RBN>
class BPTree {
	public:
		BPTree(fstream &, stringstream &, stringstream &, uint8_t, ORDER order = SEQ_SET, T order_size = 4, float fill = 0.75);
		BPTree(ifstream &);
		void insert(string);
		bool remove(string);
		void remove();
		bool check();
		void dump();

		friend Block<T,RBN>;

		bool debug = false;
		bool verbose = false;
		struct {
			struct {
				T hdr;
				T block;
				T record;
				float fill;
			} size;

			struct {
				T total = 0;
				T idx = 0;
				T seq = 0;
				T avail = 4;	//Default size of avail list
				T records = 0;
				T keys = 0;
				T fields;
			} count;

			RBN root = -1;
			RBN seq_set = -1;
			RBN avail = 0;

			struct {
				T pkey, pkey_start, pkey_end;		//AKA sort ordinal
				vector<T> size;
				vector<string> labels;
			} field;

			struct {
				uint32_t create;
				uint32_t modify;
			} time;
		} hdr;
		private:
		
		fstream &bpfile;

		void *avail_list;

		RBN newBlock();
		void deleteBlock(RBN);
		inline streampos getBlock(RBN);
		void setBlock(RBN, string);
		void initAvail();
		void writeHeader();
};

/**
 * Constructs and initializes a B+ tree.
 * @param fieldlabels A stringstream containing all the comma-separated field labels a record
 * @param fieldsizes A stringstream containing all the comma-separated field sizes of a record
 * @param pkey The index of the field which will be the primary key
 * @param order An optional argument specifying the method used to allocate the block size.  Options are BLK_SIZE, SEQ_SET, and IDX_SET.  Default is SEQ_SET.
 * @param order_size An optional argument specifying the order/size of the method chosen for the 'order' parameter.  For BLK_SIZE, this parameter takes block size in bytes.  For SEQ_SET or IDX_SET, this parameter takes the respective order size.  Default is 4.
 * @param fill This parameter is the fill percentage for each block in the tree.  Default is 0.75 (75%).
 */
template <typename T, typename RBN>
BPTree<T,RBN>::BPTree(fstream &file, stringstream &fieldlabels, stringstream &fieldsizes, uint8_t pkey,
				ORDER order, T order_size, float fill) {
	hdr.size.fill = fill;
	hdr.field.pkey = pkey;
	bpfile = file;

	//parse fields and add up sizes
	//TODO: need to calculate pkey_start and pkey_end
	hdr.size.record = 0;
	vector<string> fl; vector<T> fs; 
	string label; T fsize;
	while (fieldlabels >> label && fieldsizes >> fsize) {
		if (label.find('*') != string::npos) {
			hdr.field.pkey = fl.size();
			label.erase(std::remove(label.begin(), label.end(), '*'), label.end());
		}
		fl.push_back(label); fs.push_back(fsize);
		hdr.size.record += fsize;
		if (fieldsizes.peek() == ',') {
			fieldsizes.ignore();
		}
	}
	hdr.count.fields = fs.size();
	hdr.field.labels = fl;
	hdr.field.size = fs;

	//TODO: need to factor in block metadata like count and pointers
	if (order == BLK_SIZE) {
		hdr.size.block = order_size;
		if (hdr.size.block < hdr.size.record) {
			hdr.size.block = hdr.size.record;
			hdr.size.block += (sizeof(T) + 3*sizeof(RBN)); //Add the size of block metadata such as count, parent, previous, and next pointers
		}
		hdr.count.records = hdr.size.block / hdr.size.record;
		hdr.count.keys = hdr.size.block / sizeof(pkey);
	} else if (order == SEQ_SET) {
		hdr.count.records = order_size;
		hdr.size.block = hdr.size.record * hdr.count.records;
		hdr.count.keys = hdr.size.block / sizeof(pkey);
	} else if (order == IDX_SET) {
		hdr.count.keys = order_size;
		if (hdr.count.keys * sizeof(pkey) < hdr.size.record) {
			hdr.count.keys = ceil(hdr.size.record / sizeof(pkey)); //might need to cast to double
		}
		hdr.size.block = hdr.count.keys * hdr.size.record;
		hdr.count.records = hdr.size.block / hdr.size.record;
	}

	//Alloc and init avail list.  We may need to resize later, so we can't use new
	avail_list = malloc(hdr.size.block * hdr.count.avail);
	initAvail();

	//write header to file;
	writeHeader();
}

template <typename T, typename RBN>
void BPTree<T,RBN>::writeHeader() {
	bpfile.seekp(0);
	bpfile.write("bpt", 3);  //file identifier

	//TODO: need to make sure all outputs are a fixed size
	bpfile << hdr.size.hdr;
	bpfile << hdr.size.block;
	bpfile << hdr.size.record;
	bpfile << fixed << setprecision(2) << hdr.size.fill;

	bpfile << hdr.count.total;
	bpfile << hdr.count.idx;
	bpfile << hdr.count.seq;
	bpfile << hdr.count.avail;
	bpfile << hdr.count.records;
	bpfile << hdr.count.keys;
	bpfile << hdr.count.fields;

	bpfile << hdr.root;
	bpfile << hdr.seq_set;
	bpfile << hdr.avail;

	bpfile << hdr.field.pkey;
	for (int i = 0; i < hdr.count.fields; i++) {
		bpfile << hdr.field.size[i];
	}
	for (int i = 0; i < hdr.count.fields; i++) {
		bpfile << hdr.field.labels[i] << ",";
	}

	bpfile << hdr.time.create;
	bpfile << hdr.time.modify;
}

template <typename T, typename RBN>
BPTree<T,RBN>::BPTree(ifstream &bfile) {
}

/**
 * Inserts a record into the B+ Tree.
 * @param record A string containing the record to add
 */
template <typename T, typename RBN>
void BPTree<T,RBN>::insert(string record) {
	//TODO:
	//- extract pkey from record
	SEQ_BLK<T,RBN> *block, *next_block = NULL;
	RBN rbn;
	if (hdr.seq_set == -1) {
		hdr.seq_set = newBlock();
		hdr.count.seq++;
		hdr.count.total++;
	}

	rbn = hdr.seq_set;
	for (int i = 0; i < hdr.count.seq; i++) {
		//TODO: need to increment block counts in case add decides to split
		block = new SEQ_BLK<T,RBN>(bpfile, getBlock(rbn));
		if (block->next == -1) {
			block->add(record);
			break;
		}

		next_block = new SEQ_BLK<T,RBN>(bpfile, getBlock(block->next));

		if ( (!block->isFull() && block->pkey(record) < next_block->pkey(next_block->records[0])) ||
			(block->isFull() && record < block->records[block->records.size()-1]) ) {
			block->add(record);
			break;
		}

		rbn = block->next;
		delete block;
		block = next_block; next_block = NULL;
	}

	//TODO:
	//- check for underflow
	//	-redistribute if possible
	//	-merge if impossible
	//- check for overflow
	//	-redistribute if possible
	//	-split if impossible
	//- make blocks write themselves to file
	writeHeader();  //Update header
	bpfile.seekp(getBlock(rbn));
	bpfile << block;
}

/**
	* finds and deletes the record with the given pKey.
	* @param pKey - primary key of the record to be deleted.
	* @return returns true if record is sucessfully deleted, false otherwise.
*/
template <typename T, typename RBN>
bool BPTree<T,RBN>::remove(string pKey){
	RBN rbn;
	string deleteKey = pKey;
	bool keyFound = false;
	// if tree is empty...
	if (hdr.seq_set == -1) {
		cout << "No records to delete." << endl;
		return keyFound;
	}
	rbn = hdr.seq_set;
	SEQ_BLK<T,RBN> *block = new SEQ_BLK<T,RBN>(bpfile, getBlock(rbn));
	// Read through each record in block until matching pkey is found,
	// Or report that matching pkey is not in tree.
	while (keyFound == false){
		for (int i = 0; i < block->records.size(); i++) {
			if(block->pkey(block->records[i]) == deleteKey){
				rbn = block->rbn;
				block->remove(block->records[i]);
				keyFound = true;
			}
		}
		if (block->next != -1) {
			block = block->next;	//TODO: error: need to fetch next block
		}
		else {
			cout << "Record not in file." << endl;
			break;
		}
	}
	writeHeader();  //Update header
	bpfile.seekp(getBlock(rbn));
	bpfile << block;
	return keyFound;
}
	//TODO
	//Check for underflow
	// -redistribute if possible
	// -merge if impossible

/**
 * Allocates and returns a block from the avail_list to use.
 */
template <typename T, typename RBN>
RBN BPTree<T,RBN>::newBlock() {
	RBN next_block = atoi(getBlock(hdr.avail));

	if (next_block == -1) {
		hdr.count.avail *= 2;
		avail_list = realloc(avail_list, hdr.count.avail * hdr.size.block);
		initAvail();
	}

	next_block = atoi(getBlock(hdr.avail));

	hdr.avail = next_block;

	return hdr.avail;
}

/**
 * Deallocates and returns a block onto the avail list.
 * @param block A relative block number (RBN) to delete.
 */
template <typename T, typename RBN>
void BPTree<T,RBN>::deleteBlock(RBN block) {
	setBlock(block, to_string(hdr.avail));
	hdr.avail = block;

	if ((hdr.count.seq + hdr.count.idx) <= (hdr.count.avail * 0.25)) {
		hdr.count.avail /= 2;
		avail_list = realloc(avail_list, hdr.count.avail * hdr.size.block); // BUG! Will truncate avail list if there are used blocks towards the end
	}
}

/**
 * Returns the file position to first character in the block
 * @param block A relative block number (RBN) to fetch the file position of
 */
template <typename T, typename RBN>
inline streampos BPTree<T,RBN>::getBlock(RBN block) {
	return hdr.size.hdr + (hdr.size.block * block);
}

/**
 * Overwrite the block with the specified text.
 * @param block The relative block number (RBN) to overwrite
 * @param str The text that will overwrite the block's contents
 */
template <typename T, typename RBN>
void BPTree<T,RBN>::setBlock(RBN block, string str) {
}

/**
 * Initializes new free blocks in the avail list.  Call whenever the avail list gets bigger.
 */
template <typename T, typename RBN>
void BPTree<T,RBN>::initAvail() {
	for (int i = hdr.avail; i < hdr.count.avail-1; i++) {
		setBlock(i, to_string(i+1));
	}
	setBlock(hdr.count.avail-1, "-1");
}

/**
 * Dumps contents of the tree to cout
 */
template <typename T, typename RBN>
void BPTree<T,RBN>::dump() {
	if (hdr.seq_set == -1) {
		cout << "Tree is Empty\n";
		return;
	}
	
	RBN rbn = hdr.seq_set;
	while (rbn != -1) {
		SEQ_BLK<T, RBN> loopBlk(bpfile, getBlock(rbn));
		cout << '[';
		for (auto i = 0; i < loopBlk.records.size(); i++) {
			cout << loopBlk.records[i];
			if ((i+1) >= loopBlk.records.size())
				cout << " | ";
		}
		cout << ']';
		rbn = loopBlk.next;
		if (rbn != -1)  //TODO: error: need to fetch next block
			cout << " -> ";
	}
	cout << '\n';
}

#endif
