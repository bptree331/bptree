#include <sstream>
#include <argp.h>

using namespace std;

//TODO:
//-Need to add options for optional arguments to BPTree

static struct argp_option options[] = {
	{"batch",	'b',	0,		OPTION_ARG_OPTIONAL,	"Run program in batch mode"},
	{"bpfile",	'f',	"FILE",		0,			"B+ tree file to use"},
	{"debug",	'd',	0,		OPTION_ARG_OPTIONAL,	"Produce debug output"},
	{"fill",	'p',	"PERCENT",	0,			"Block fill percentage"},
	{"fieldlabels", 'l',	0,		0,			"Comma separated list of field labels"},
	{"fieldsizes",	's',	0,		0,			"Comma separated list of field sizes"},
	{"output",	'o',	"FILE",		0,			"Output the tree to file"},
	{"verbose",	'v',	0,		OPTION_ARG_OPTIONAL,	"Produce verbose output"},
	{"remove",	'r',	"FILE",		0,			"A file containing a B+ Tree formatted list of records to delete from the tree"},
	{"insert",	'i',	"FILE",		0,			"A file containing a B+ Tree formatted list of records to insert into the tree"},
};

struct arguments {
	bool debug, verbose, batch;
	char *bpfilename;
	char *insert;
	char *remove;
	stringstream fieldlabels, fieldsizes;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *args = (arguments *) state->input;

	switch (key) {
		case 'b':
			args->batch = true;
			break;
		case 'd':
			args->debug = true;
			break;
		case 'f':
			args->bpfilename = arg;
			break;
		case 'l':
			args->fieldlabels << arg;
			break;
		case 's':
			args->fieldsizes << arg;
			break;
		case 'i':
			args->insert = arg;
			break;
		case 'r':
			args->remove = arg;
			break;
		case 'v':
			args->verbose = true;
			break;
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

static struct argp argp = { options, parse_opt, NULL, NULL};


