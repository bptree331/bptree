/****************************************************************************
makeTestFile.cpp
Given a source data file and a test data file name, and optionally a number of 
records, this script will fill the test data file with a set of random records

Terminal Syntax: makeTestFile [option 1] ... [source file] [destination file] 
Options:
	-c=COUNT, record count
		Indicates the number of records that the test file will consist of.

IE for a file of 10 records
	makeTestFile -c 10 source.txt output.txt
For a file of a number of records randomly selected in the "reasonable" range
	makeTestFile source.txt output.txt

source file: Contains the source data that the test file will be made from

destination file: Will be where the generated test data is sent.

Written by: Brent Clapp								12/12/2018
*****************************************************************************/
#include <fstream>	//For file processing
#include "getopt.h" //For the "getopt" methods
#include <stack> //Used in the process of choosing data elements to include
#include <cctype> //Used for isdigit() method
#include <cstdlib>  //Using rand() method, atoi()
#include <ctime>	//Seeding RNG using time
#include <string> //Storing file names as strings
#include <iostream>
using namespace std;

//constants
const int MINDEFAULT = 20;
const int MAXDEFAULT = 500;
const int NUMFILES = 2;	//Number of files that will be passed via terminal

int main(int argc, char *argv[]){
	int currOpt;	//Will contain the current option in the loop
	int recCount = -1; //Will hold the number of records, if necessary
	int currIndex;	//Will represent current index while inserting into outFile
	int lineCount = 0; //Will hold the number of data lines in the source file
	int junkLines = 0; //Will hold the number of non-data lines in the file
	string outFileName;	//Will hold the name of the test data file
	string inFileName;	//Will hold the name of the source data file
	srand(time(NULL)); //Seed RNG with time
	//Individual fields to be used:
	unsigned zipCode;
	string city;
	string state;
	string county;
	double latitude;
	double longitude;
	
	//Receive options:
	while((currOpt = getopt(argc, argv, "c:"))!= -1){
		switch(currOpt){ //Evaluating current argument
			case 'c':
				recCount = atoi(optarg);	//Record value of argument count
				break;
			case '?':
				if(optopt == 'c')
					cerr << "Error, option -c requires an argument. \n";
				else
					cerr << "Invalid option character.\n";
				return 18; //Error code 18
			default:
				exit(1000);
		}//end switch
	}//End while(currOpt)
	
	if(optind != argc - NUMFILES){
		cerr << "Invalid number of arguments.\n" << endl;
		return 25; //Error code 25
	}//end if
	
	//Receive file names:
	inFileName = argv[optind++];//Receive input file, move to next index
	outFileName = argv[optind];	//Receive output file
	
	//File processing:
	ifstream inFile(inFileName); //Opening the source file
	ofstream outFile(outFileName); //Creating and opening the output file
	
	if(!outFile){
		cerr << "Outfile is broken, aborting" << endl;
		exit(50);
	}//end if
	if(!inFile){
		cerr << "Infile is broken, aborting" << endl;
		exit(150);
	}//end if
	
	//If no passed number of records, generate a number to use
	if(recCount == -1)
		recCount = (rand() % (MAXDEFAULT - MINDEFAULT) ) + MINDEFAULT; 
	
	//Need to determine where records actually start in inFile (Where the first numeric val is)
	int ch;	//Will be used to hold the character in following loops
    while (!(isdigit(inFile.peek()))){
    	ch = inFile.get();
        if ('\n' == ch)
            junkLines++;
	}//end while	

	//Need to determine number of data entries in input file to take random values
    while (!inFile.eof()){
    	ch = inFile.get();
        if ('\n' == ch)
            lineCount++;
	}//end while

	//lineCount should have the number of records in the input file
	
	stack<int> s; //Will be used to hold all of the indicies of records to be placed in the output file
	bool selected[lineCount];	//Will tell us whether or not each record has already been selected.
	
	for(int i = 0; i < recCount; i++){	//For each record
		currIndex = rand() % lineCount;	//Grab an index between 0 and lineCount-1
		while(selected[currIndex])			//Make sure we aren't taking duplicate records
			currIndex = rand() % lineCount;
		s.push(currIndex);				//Record the index in the stack
		selected[currIndex] = true; //Mark the index as selected.
	}//end for(i)
	
	while(!s.empty()){
		currIndex = s.top();
		s.pop();		//Get/Remove next element
		inFile.clear(); //Clear the flags from previous traversing
		inFile.seekg(ios::beg); //Put the pointer back to the start
		//Go through file until index of that line is reached
		for(int i = 0; i < (currIndex + junkLines); i++) //junkLines added to avoid the offset of those lines causing problems
			inFile.ignore(256, '\n');
		
		
		//Now we're at the location in inFile. Read specific fields, output them formatted
		inFile >> zipCode;
		inFile >> city;	
		inFile >> state;
		inFile >> county;
		inFile >> latitude;
		inFile >> longitude;
	
		//Output them formatted
		outFile << zipCode << ',';
		outFile << city << ',';
		outFile << state << ',';
		outFile << county << ',';
		outFile << latitude << ',';
		outFile << longitude << endl; //Move to the next record's line
	
	}//end while
	
	inFile.close();	//Close the files
	outFile.close();
	
	return 0;
}//end main